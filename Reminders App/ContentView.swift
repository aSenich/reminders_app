//
//  ContentView.swift
//  Reminders App
//
//  Created by Mark Senich on 6/7/19.
//  Copyright © 2019 Austin senich. All rights reserved.
//

import SwiftUI

struct ContentView : View {
    var body: some View {
        Text("Hello World")
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
